// restore nuget packages
// build
echo "RECIPIENTS: ${RECIPIENTS}"

node("vs2013 && nuget && nunit")
{
	checkPreconditionsForTest()

	stage("CHECKOUT")
	{
		deleteDir()
		checkout scm
	}

	stage("BUILD")
	{
		nugetRestore "src"
		msbuild getSolutionFile()
	}

	stage("TEST")
	{
		executeTests "Tests"
		publishTestResult()
		notify RECIPIENTS
	}

	stage("PACK")
	{
		bat "package.bat"
	}
}
