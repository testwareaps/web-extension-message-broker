function messageListener(msg) {
  console.log("background script received message");
  
  sendNativeMessage(msg);
}

function navigation_OnCompleted(e) {
	if(e.frameId !== 0)
		return;
	
	var activateTabHtml;
	
	chrome.tabs.getSelected(null, function(tab) {
		// Send a request to the content script.
		chrome.tabs.sendRequest(tab.id, {action: "getDOM"}, function(response) {
		  activateTabHtml = response.documentHTML;
		});
	});
	
  var msg = {
		messageType : "Navigation",
		url : e.url,
		documentHTML : document.body.outerHTML,
	};
  
  sendNativeMessage(msg);
}

var port = chrome.runtime.connectNative("testware.web.webextensionmessagebroker");

function sendNativeMessage(msg) {
	console.log(msg.messageType);
	
  port.postMessage(msg);
}

chrome.runtime.onMessage.addListener(messageListener);
chrome.webNavigation.onCompleted.addListener(navigation_OnCompleted);