function setup() {
	chrome.extension.onRequest.addListener(backgroundRequestListener);
	document.addEventListener("click", documentClicked);
	
	var observer = new MutationObserver(mutationObserverCallback);
	
	observer.observe(document.body,
	{
	  attributes: true,
	  attributeOldValue: true,
	  characterData: false,
	  characterDataOldValue: false,
	  childList: true,
	  subtree: true
	});
	
	Observers.push(observer);
	
	window.onbeforeunload = disconnectAllOberservers;
}

function backgroundRequestListener(request, sender, sendResponse)
{
	if (request.action == "getDOM")
		sendResponse({documentHTML: JSON.stringify(document.body.outerHTML)});
	else
		sendResponse({}); // Send nothing..
}

function documentClicked(e) {
	console.log("Click");
	
	var clickedElement = findClickableElement(e.target);
	
	if(clickedElement !== null)
		notifyExtensionElementClicked(clickedElement);
}

function callBackground(messageType, elementXPath, mutations) {
	console.log("content script sending message");
	
	var msg = {
		messageType : messageType,
		elementXPath : elementXPath,
		url : window.location.href,
		documentHTML : JSON.stringify(document.body.outerHTML),
		mutations : mutations
	};
  
	chrome.runtime.sendMessage(msg);
}

function notifyExtensionElementClicked(e) {	
	var xPathOfTarget = createXPathFromElement(e);
	
	callBackground("Click", xPathOfTarget);
}

function createXPathFromElement(element) {   
  if(element.id !== "" && !duplicateIdsExistsInDocument(element.id)) {
    return "//" + element.tagName + "[@id='" + element.id + "']";
  } else {
    var parentElement = element.parentElement;  
  
    if(parentElement === null)
      return "/" + element.tagName;
  
    var childElementsOfParent = Array.prototype.slice.call(parentElement.children, 0);
  
    var childElementsOfSameType = childElementsOfParent.filter(function(e) {
      return e.tagName == element.tagName;
    });
  
    if(childElementsOfSameType.length == 1) {
      return createXPathFromElement(parentElement) + "/" + element.tagName;
    } else {
      for(var i = 0, count = childElementsOfSameType.length; i < count; i++)
        if(childElementsOfSameType[i] == element)
          return createXPathFromElement(parentElement) + "/" + element.tagName + "[" + (i + 1) + "]";
    }
  }
}

function duplicateIdsExistsInDocument(id) {
  var idSelector = "[id='" + id + "']";
  
  var elementsWithId = this.document.querySelectorAll(idSelector);
  
  return elementsWithId.length > 1;
}

function findClickableElement(e) {
	if (!e.tagName)
		return null;
	
	var tagName = e.tagName.toLowerCase();
	var type = e.type;
	
	if (e.hasAttribute("onclick") || e.hasAttribute("href") || tagName == "button" || tagName == "a" ||
		 (tagName == "input" &&
		 (type == "submit" || type == "button" || type == "image" || type == "radio" || type == "checkbox" || type == "reset")))
		return e;
	else {
		if (e.parentNode !== null)
			return this.findClickableElement(e.parentNode);
		else
			return null;
	}
}

function mutationObserverCallback(mutations, observer) {
	//Ignore mutations consisting of only text node changes
	if(!mutationsContainsNonTextNodes(mutations))
		return;

	callBackground("DOMChange", null, parseMutations(mutations));
}

function parseMutations(mutations) {
	var parsedMutations = [];
	
	mutations.forEach(function(mu) {
		var addedParsedNodes = [];
		var removedParsedNodes = [];
		
		mu.addedNodes.forEach(function(node) {
			addedParsedNodes.push({
				nodeXPath : createXPathFromElement(node)
			});
		});
		
		mu.removedNodes.forEach(function(node) {
			removedParsedNodes.push({
				nodeXPath : createXPathFromElement(node)
			});
		});
		
		var parsedMutation = {
			type : mu.type,
			addedNodes : addedParsedNodes,
			removedNodes : removedParsedNodes,
			attributeName : mu.attributeName,
			targetXPath : createXPathFromElement(mu.target)
		};
		
		if(mu.attributeName !== null) {
			parsedMutation.attributeNewValue = mu.target.getAttribute(mu.attributeName);
			parsedMutation.attributeOldValue = mu.oldValue;
		}
		
		parsedMutations.push(parsedMutation);
	});
	
	return parsedMutations;
}

function mutationsContainsNonTextNodes(mutations) {
	for(var i = 0; i < mutations.length; i++) {
		var mutation = mutations[i];
		
		if(mutation.addedNodes.length === 0 && mutation.removedNodes.length === 0)
			if(mutation.target.nodeType != Node.TEXT_NODE)
				return true;
		
		for(var j = 0; j < mutation.addedNodes.length; j++)
			if(mutation.addedNodes[j].nodeType != Node.TEXT_NODE)
				return true;
			
		for(var k = 0; k < mutation.removedNodes.length; k++)
			if(mutation.removedNodes[k].nodeType != Node.TEXT_NODE)
				return true;
	}
	
	return false;
}

function disconnectAllOberservers() {
	console.log("Disconnect");
	
	for(var i = 0; i < Observers.length; i++)
		Observers[i].disconnect();
}

MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
Observers = [];

setup();