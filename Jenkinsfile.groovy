node('vs2013')
{
	notifyOnErrorStage('CHECKOUT')
	{
		deleteDir()
		checkout scm
		nugetRestore 'src'
	}

	sonarQubeAnalysis()
	{
		notifyOnErrorStage('BUILD')
		{
			msbuild getSolutionFile()
		}

		errorHandlingStage('TEST') 
		{
			executeVSTest()
		}
	}

	errorHandlingStage('PUBLISH-RESULT')
	{
		publishVSTestResult()
	}

	stage('NOTIFY')
	{
		notify RECIPIENTS
	}
}