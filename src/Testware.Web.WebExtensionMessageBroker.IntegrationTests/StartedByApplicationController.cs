﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Testware.Web.Core.WebDriver.ApplicationController;
using Testware.Web.WebExtensionMessageBroker.Client;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.IntegrationTests
{
    public class StartedByApplicationController
    {
        WebDriverApplicationController _controller;

        [SetUp]
        public void Setup()
        {
            _controller = new ChromeApplicationController();

            var extensionInitializer = new ChromeWebExtensionInitializer();

            _controller.Start(extensionInitializer);
        }

        [Test]
        public void NavigationShouldResultInOneMessageFromMessageBroker()
        {
            var brokerClient = new BrokerClient();
            brokerClient.StartAsync();

            _controller.Driver.Navigate().GoToUrl("chrome://about/");

            List<WebExtensionMessage> messages = null;

            var getMessagesTaks = Task.Factory.StartNew(() =>
            {
                messages = brokerClient.GetMessages().ToList();
            });

            _controller.Stop();

            getMessagesTaks.Wait(TimeSpan.FromSeconds(5));

            Assert.AreEqual(1, messages.Count);
        }
    }
}
