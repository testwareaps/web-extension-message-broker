﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Testware.Web.WebExtensionMessageBroker.Broker;
using Testware.Web.WebExtensionMessageBroker.Client;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.IntegrationTests
{
    public class PingTest : IDisposable
    {
        BrokerServer _server;
        BrokerClient _client;
        BlockingCollection<WebExtensionMessage> _serverQueue;

        [SetUp]
        public void Setup()
        {
            _serverQueue = new BlockingCollection<WebExtensionMessage>();

            _server = new BrokerServer(_serverQueue);
            _server.StartAsync(CancellationToken.None);

            _client = new BrokerClient();
            _client.StartAsync();
        }

        [Test]
        public void SentMessagesCountShouldEqualReceivedMessageCount()
        {
            int messageCount = 30;

            var receivedMessages = new List<WebExtensionMessage>();

            var clientTask = Task.Factory.StartNew(() =>
            {
                foreach (var msg in _client.GetMessages())
                    receivedMessages.Add(msg);
            });

            Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < messageCount; i++)
                {
                    _serverQueue.Add(new WebExtensionMessage("Ping", null, null, null, null));

                    Thread.Sleep(100);
                }

                _serverQueue.CompleteAdding();
            });

            clientTask.Wait();

            Assert.AreEqual(messageCount, receivedMessages.Count);
        }

        [TearDown]
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources  
                if (_serverQueue != null)
                {
                    _serverQueue.Dispose();
                    _serverQueue = null;
                }
            }
        }
    }
}
