﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Support.UI;
using Testware.Web.WebExtensionMessageBroker.Client;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.IntegrationTests
{
    public class AutomatedNavigationTest : IDisposable
    {
        private RandomCrawler _crawler;
        private BrokerClient _client;

        private List<WebExtensionMessage> _messages;

        Task _clientConsumerTask;

        [SetUp]
        public void Setup()
        {
            _crawler = new RandomCrawler();

            _client = new BrokerClient();
            _client.StartAsync();

            _messages = new List<WebExtensionMessage>();

            _clientConsumerTask = new Task(() =>
            {
                foreach (var msg in _client.GetMessages().Where(m => m.MessageType == "Click"))
                {
                    Console.WriteLine("Clicked: " + msg.ElementXPath + " - " + msg.Url);

                    _messages.Add(msg);
                }
            });

            _clientConsumerTask.Start();
        }

        [Test]
        public void RojaboClickCount()
        {
            var clickCount = 25;

            _crawler.CrawlSite("http://www.rojabo.com/", "http://www.rojabo.com", clickCount);

            _crawler.Quit();

            _clientConsumerTask.Wait();

            Assert.AreEqual(clickCount, _messages.Count);
        }

        [Test]
        public void GcsdClickCount()
        {
            Action<IWebDriver> prepare = d =>
            {
                d.Navigate().GoToUrl("http://rtgs.testenv.dn:8080/web/login.do");

                var txtUserId = d.FindElement(By.Name("userId"));
                var txtPassword = d.FindElement(By.Name("userPwd"));
                var cmdLogin = d.FindElement(By.Id("btnLoginLabel"));

                txtUserId.SendKeys("DNCEL");
                txtPassword.SendKeys("1234");

                cmdLogin.Click();

                var wait = new WebDriverWait(d, TimeSpan.FromSeconds(10));
                wait.Until(RandomCrawler.DocumentIsComplete);

                var gcsdLink = d.FindElement(By.LinkText("GCSD"));

                gcsdLink.Click();

                var currentWindowHandle = d.CurrentWindowHandle;
                d.SwitchTo().Window(d.WindowHandles.First(h => h != currentWindowHandle));

                wait.Until(RandomCrawler.DocumentIsComplete);
            };

            var clickCount = 25;

            _crawler.CrawlSite(prepare, "", clickCount);

            _crawler.Quit();

            _clientConsumerTask.Wait();

            Assert.AreEqual(clickCount + 2, _messages.Count);
        }

        [TearDown]
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources  
                if (_clientConsumerTask != null)
                {
                    _clientConsumerTask.Dispose();
                    _clientConsumerTask = null;
                }

                if (_crawler != null)
                {
                    _crawler.Dispose();
                    _crawler = null;
                }
            }
        }
    }

    public class RandomCrawler : IDisposable
    {
        private IWebDriver _driver;

        public RandomCrawler()
        {
            var options = new ChromeOptions();
            options.AddArgument(@"--load-extension=C:\Kode\Testware\web-extension-message-broker\WebExtension");

            var driver = new ChromeDriver(options);

            _driver = new EventFiringWebDriver(driver);
        }

        public void CrawlSite(string url, string linkDomainFilter, int clickCount)
        {
            Action<IWebDriver> prepare = d => d.Navigate().GoToUrl(url);

            this.CrawlSite(prepare, linkDomainFilter, clickCount);
        }

        public void CrawlSite(Action<IWebDriver> prepare, string linkDomainFilter, int clickCount)
        {
            prepare(_driver);

            var resetUrl = _driver.Url;
            
            var rnd = new Random();
            var currentClickCount = 0;

            while (currentClickCount < clickCount)
            {
                var wait = new MessageBrokerWait();
                wait.Wait(TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(10));

                var links = _driver.FindElements(By.XPath(".//a[not(ancestor::div[contains(@style,'display:none')]) and not(ancestor::div[contains(@style,'display: none')])]")).Where(e => e.Displayed && e.GetAttribute("href").Contains(linkDomainFilter)).ToList();
                var buttons = _driver.FindElements(By.TagName("button")).Where(e => e.Displayed).ToList();

                var navigationElements = links.Concat(buttons).ToList();

                if (navigationElements.Count == 0)
                {
                    _driver.Navigate().GoToUrl(resetUrl);

                    continue;
                }

                var clickLink = navigationElements[rnd.Next(navigationElements.Count)];

                Console.WriteLine("Click perform: " + clickLink.Text);

                new Actions(_driver).MoveToElement(clickLink).Click().Perform();

                //var wait = new WebDriverWait(_Driver, TimeSpan.FromSeconds(10));
                //wait.Until(DocumentIsComplete);

                //Thread.Sleep(2000);

                currentClickCount++;
            }
        }

        public void Quit()
        {
            _driver.Quit();
        }

        public static Func<IWebDriver, bool> DocumentIsComplete
        {
            get
            {
                return d =>
                {
                    if (d == null)
                        throw new ArgumentNullException();

                    try
                    {
                        return ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete");
                    }
                    catch (InvalidOperationException)
                    {
                        //Ignore javascript errors
                        return false;
                    }
                };
            }
        }

        [TearDown]
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources  
                if (_driver != null)
                {
                    _driver.Dispose();
                    _driver = null;
                }
            }
        }
    }
}
