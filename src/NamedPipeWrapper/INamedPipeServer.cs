﻿using System;
namespace NamedPipeWrapper
{
    public interface INamedPipeServer<TReadWrite> : INamedPipeServer<TReadWrite, TReadWrite>
        where TReadWrite : class { }

    public interface INamedPipeServer<TRead, TWrite>
        where TRead : class
        where TWrite : class
    {
        int ClientsConnected { get; }

        event ConnectionEventHandler<TRead, TWrite> ClientConnected;
        event ConnectionEventHandler<TRead, TWrite> ClientDisconnected;
        event ConnectionMessageEventHandler<TRead, TWrite> ClientMessage;
        event PipeExceptionEventHandler Error;

        void PushMessage(TWrite message);

        void PushMessage(TWrite message, string clientName);

        void Start();

        void Stop();
    }
}
