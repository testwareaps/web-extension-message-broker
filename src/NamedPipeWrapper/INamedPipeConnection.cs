﻿using System;
namespace NamedPipeWrapper
{
    public interface INamedPipeConnection<TRead, TWrite>
        where TRead : class
        where TWrite : class
    {
        string Name { get; }

        bool IsConnected { get; }

        event ConnectionMessageEventHandler<TRead, TWrite> ReceiveMessage;
        event ConnectionEventHandler<TRead, TWrite> Disconnected;
        event ConnectionExceptionEventHandler<TRead, TWrite> Error;

        void Open();

        void Close();

        void PushMessage(TWrite message);
    }
}
