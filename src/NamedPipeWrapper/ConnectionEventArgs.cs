﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NamedPipeWrapper
{
    public class ConnectionEventArgs<TRead, TWrite> : EventArgs
        where TRead : class
        where TWrite : class
    {
        public NamedPipeConnection<TRead, TWrite> Connection { get; private set; }

        public ConnectionEventArgs(NamedPipeConnection<TRead, TWrite> connection)
        {
            if (connection == null)
                throw new ArgumentNullException("connection");

            this.Connection = connection;
        }
    }
}
