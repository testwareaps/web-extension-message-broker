﻿using System;

namespace NamedPipeWrapper
{
    public interface INamedPipeClient<TReadWrite> : INamedPipeClient<TReadWrite, TReadWrite>
        where TReadWrite : class { }

    public interface INamedPipeClient<TRead, TWrite>
        where TRead : class
        where TWrite : class
    {
        bool AutoReconnect { get; set; }

        event ConnectionEventHandler<TRead, TWrite> Disconnected;
        event PipeExceptionEventHandler Error;
        event ConnectionMessageEventHandler<TRead, TWrite> ServerMessage;

        void PushMessage(TWrite message);
        
        void Start();

        void Stop();

        void WaitForConnection();

        void WaitForConnection(int millisecondsTimeout);

        void WaitForConnection(TimeSpan timeout);

        void WaitForDisconnection();

        void WaitForDisconnection(int millisecondsTimeout);

        void WaitForDisconnection(TimeSpan timeout);
    }
}
