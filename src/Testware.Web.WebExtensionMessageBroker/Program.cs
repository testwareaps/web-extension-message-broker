﻿using System;
using System.Collections.Concurrent;
using System.Windows.Forms;
using Testware.Web.WebExtensionMessageBroker.Broker;
using Testware.Web.WebExtensionMessageBroker.Broker.UI;
using Testware.Web.WebExtensionMessageBroker.Message;
using Testware.Web.WebExtensionMessageBroker.Utils;

namespace Testware.Web.WebExtensionMessageBroker
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var incommingQueue = new BlockingCollection<WebExtensionMessage>();
            var uiMessages = new BlockingCollection<WebExtensionMessage>(new ConcurrentFixedSizedQueue<WebExtensionMessage>(100));

            var webExtensionListener = new WebExtensionListener(incommingQueue, uiMessages);
            var brokerServer = new BrokerServer(incommingQueue);

            var frmMain = new MainForm(webExtensionListener, brokerServer, uiMessages);

            var application = new MessageBrokerApplication(webExtensionListener, brokerServer, frmMain);

            application.Start();
        }
    }
}
