﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Testware.Web.WebExtensionMessageBroker.Utils
{
    internal class ConcurrentFixedSizedQueue<T> : IProducerConsumerCollection<T>
    {
        private readonly ConcurrentQueue<T> _queue = new ConcurrentQueue<T>();

        public int Limit { get; private set; }

        public int Count { get { return _queue.Count; } }

        public ConcurrentFixedSizedQueue(int limit)
        {
            this.Limit = limit;
        }

        public void Enqueue(T obj)
        {
            _queue.Enqueue(obj);
            lock (_queue)
            {
                T overflow;

                while (_queue.Count > Limit)
                    _queue.TryDequeue(out overflow) ;
            }
        }

        public void CopyTo(T[] array, int index)
        {
            _queue.CopyTo(array, index);
        }

        public T[] ToArray()
        {
            return _queue.ToArray();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _queue.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        void ICollection.CopyTo(Array array, int index)
        {
            ((IProducerConsumerCollection<T>)_queue).CopyTo(array, index);
        }

        bool ICollection.IsSynchronized
        {
            get { return ((IProducerConsumerCollection<T>)_queue).IsSynchronized; }
        }

        object ICollection.SyncRoot
        {
            get { return ((IProducerConsumerCollection<T>)_queue).SyncRoot; }
        }

        bool IProducerConsumerCollection<T>.TryAdd(T item)
        {
            Enqueue(item);
            return true;
        }

        bool IProducerConsumerCollection<T>.TryTake(out T item)
        {
            return ((IProducerConsumerCollection<T>)_queue).TryTake(out item);
        }
    }
}
