﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Windows.Forms;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.Broker.UI
{
    internal partial class MainForm : Form
    {
        #region Declarations

        private bool _allowVisible;
        private bool _allowClose;

        private readonly IWebExtensionListener _listener;
        private readonly IBrokerServer _broker;
        private readonly BlockingCollection<WebExtensionMessage> _lastMessages;

        #endregion

        #region Events

        public event EventHandler<KeepOpenChangedEventArgs> KeepOpenChanged;

        #endregion

        #region .ctors

        public MainForm()
        {
            InitializeComponent();

            this.appNotifyIcon.Icon = this.Icon;
            this.appNotifyIcon.Text = this.Text;

            this.contextMenuItemOpen.Click += this.contextMenuItemOpen_Click;
            this.contextMenuItemClose.Click += this.contextMenuItemClose_Click;
        }
        internal MainForm(IWebExtensionListener listener, IBrokerServer broker, BlockingCollection<WebExtensionMessage> lastMessages)
            : this()
        {
            if (listener == null)
                throw new ArgumentNullException("listener");
            if (broker == null)
                throw new ArgumentNullException("broker");
            if (lastMessages == null)
                throw new ArgumentNullException("lastMessages");

            _listener = listener;
            _broker = broker;
            _lastMessages = lastMessages;
        }

        #endregion

        #region Event handlers

        private void _Broker_ClientConnectionCountChanged(object sender, EventArgs e)
        {
            this.UpdateClientCount();
        }

        private void _Listener_ConnectionClosed(object sender, EventArgs e)
        {
            this.Invoke((Action)UpdateUi);
        }

        void _Listener_MessageReceived(object sender, EventArgs e)
        {
            this.Invoke((Action)UpdateUi);
        }

        private void tabsMain_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == this.tabMessages && this.chkAutoRefresh.Checked) {
                    this.UpdateMessageListView();
            }
        }

        private void chkStayOnTop_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = this.chkStayOnTop.Checked;
        }

        private void chkAutoRefresh_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkAutoRefresh.Checked)
                this.UpdateMessageListView();
        }

        private void chkKeepOpen_CheckedChanged(object sender, EventArgs e)
        {
            this.OnKeepOpenChanged(new KeepOpenChangedEventArgs(this.chkKeepOpen.Checked));
        }

        #endregion

        #region Event raisers

        private void OnKeepOpenChanged(KeepOpenChangedEventArgs e)
        {
            var tmp = this.KeepOpenChanged;

            if (tmp != null)
                tmp(this, e);
        }

        #endregion

        #region Form display behavior

#if !DEBUG
        protected override void SetVisibleCore(bool value)
        {
            if (!_AllowVisible)
            {
                value = false;
                if (!this.IsHandleCreated) CreateHandle();
            }
            base.SetVisibleCore(value);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (!_AllowClose)
            {
                this.Hide();
                e.Cancel = true;
            }
            base.OnFormClosing(e);
        }
#endif

        private void contextMenuItemOpen_Click(object sender, EventArgs e)
        {
            this.EnsureFormVisible();
        }

        private void contextMenuItemClose_Click(object sender, EventArgs e)
        {
            _allowClose = true;
            Application.Exit();
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                this.appNotifyIcon.Visible = true;
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                this.appNotifyIcon.Visible = false;
            }
        }

        private void appNotifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.EnsureFormVisible();
        }

        public void EnsureFormVisible()
        {
            _allowVisible = true;
            this.Show();

            //Small hack to ensure the form is brought to the front. BringToFront() doesn't work for some reason
            this.TopMost = true;
            this.TopMost = false;
        }

        #endregion

        #region Private methods

        private void UpdateUi()
        {
            var lastMessage = _lastMessages.Last();

            this.txtReceivedMessageCount.Text = _listener.MessagesReceived.ToString();
            this.txtLastMessageReceived.Text = lastMessage.Received.ToString("yyyy-MM-dd HH:mm:ss.fff");

            this.txtWebExtConnectionStatus.Text = _listener.IsRunning ? "Connected" : "Disconnected";

            if (this.chkAutoRefresh.Checked && this.tabsMain.SelectedTab == this.tabMessages)
                this.UpdateMessageListView();
        }

        private void UpdateClientCount()
        {
            this.txtConnectedClientCount.Text = _broker.ConnectedClients.ToString();
        }

        public void UpdateMessageListView()
        {
            this.lstLastReceivedMessages.Items.Clear();

            this.lstLastReceivedMessages.BeginUpdate();

            foreach (var m in _lastMessages.OrderByDescending(m => m.Received))
            {
                var lvItem = new ListViewItem(m.Received.ToString("HH:mm:ss.fff"));

                lvItem.SubItems.Add(m.MessageType);

                this.lstLastReceivedMessages.Items.Add(lvItem);
            }

            this.lstLastReceivedMessages.EndUpdate();
        }

        #endregion

        private void MainForm_Load(object sender, EventArgs e)
        {
            _listener.MessageReceived += _Listener_MessageReceived;
            _listener.ConnectionClosed += _Listener_ConnectionClosed;

            _broker.ClientConnected += _Broker_ClientConnectionCountChanged;
            _broker.ClientDisconnected += _Broker_ClientConnectionCountChanged;
        }
    }
}
