﻿namespace Testware.Web.WebExtensionMessageBroker.Broker.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "88:88:88.888",
            "Click"}, -1);
            this.appNotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.appNotifyIconContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuItemOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuItemClose = new System.Windows.Forms.ToolStripMenuItem();
            this.lblReceivedMessageCount = new System.Windows.Forms.Label();
            this.txtReceivedMessageCount = new System.Windows.Forms.Label();
            this.lblReceivedMessageErrorCount = new System.Windows.Forms.Label();
            this.tabsMain = new System.Windows.Forms.TabControl();
            this.tabStatus = new System.Windows.Forms.TabPage();
            this.txtConnectedClientCount = new System.Windows.Forms.Label();
            this.lblConnectedClientCount = new System.Windows.Forms.Label();
            this.txtWebExtConnectionStatus = new System.Windows.Forms.Label();
            this.lblWebExtConnectionStatus = new System.Windows.Forms.Label();
            this.txtLastMessageReceived = new System.Windows.Forms.Label();
            this.lblLastMessageReceived = new System.Windows.Forms.Label();
            this.txtReceivedMessageErrorCount = new System.Windows.Forms.Label();
            this.tabMessages = new System.Windows.Forms.TabPage();
            this.chkAutoRefresh = new System.Windows.Forms.CheckBox();
            this.lstLastReceivedMessages = new System.Windows.Forms.ListView();
            this.colMessageTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMessageEvent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chkStayOnTop = new System.Windows.Forms.CheckBox();
            this.chkKeepOpen = new System.Windows.Forms.CheckBox();
            this.appNotifyIconContextMenuStrip.SuspendLayout();
            this.tabsMain.SuspendLayout();
            this.tabStatus.SuspendLayout();
            this.tabMessages.SuspendLayout();
            this.SuspendLayout();
            // 
            // appNotifyIcon
            // 
            this.appNotifyIcon.ContextMenuStrip = this.appNotifyIconContextMenuStrip;
            this.appNotifyIcon.Text = "Web Extension Message Broker";
            this.appNotifyIcon.Visible = true;
            this.appNotifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.appNotifyIcon_MouseDoubleClick);
            // 
            // appNotifyIconContextMenuStrip
            // 
            this.appNotifyIconContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextMenuItemOpen,
            this.contextMenuItemClose});
            this.appNotifyIconContextMenuStrip.Name = "appNotifyIconContextMenuStrip";
            this.appNotifyIconContextMenuStrip.Size = new System.Drawing.Size(104, 48);
            // 
            // contextMenuItemOpen
            // 
            this.contextMenuItemOpen.Name = "contextMenuItemOpen";
            this.contextMenuItemOpen.Size = new System.Drawing.Size(103, 22);
            this.contextMenuItemOpen.Text = "Open";
            // 
            // contextMenuItemClose
            // 
            this.contextMenuItemClose.Name = "contextMenuItemClose";
            this.contextMenuItemClose.Size = new System.Drawing.Size(103, 22);
            this.contextMenuItemClose.Text = "Close";
            // 
            // lblReceivedMessageCount
            // 
            this.lblReceivedMessageCount.AutoSize = true;
            this.lblReceivedMessageCount.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceivedMessageCount.Location = new System.Drawing.Point(6, 8);
            this.lblReceivedMessageCount.Name = "lblReceivedMessageCount";
            this.lblReceivedMessageCount.Size = new System.Drawing.Size(122, 13);
            this.lblReceivedMessageCount.TabIndex = 1;
            this.lblReceivedMessageCount.Text = "Received messages:";
            // 
            // txtReceivedMessageCount
            // 
            this.txtReceivedMessageCount.Location = new System.Drawing.Point(195, 8);
            this.txtReceivedMessageCount.Name = "txtReceivedMessageCount";
            this.txtReceivedMessageCount.Size = new System.Drawing.Size(99, 13);
            this.txtReceivedMessageCount.TabIndex = 2;
            this.txtReceivedMessageCount.Text = "0";
            this.txtReceivedMessageCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblReceivedMessageErrorCount
            // 
            this.lblReceivedMessageErrorCount.AutoSize = true;
            this.lblReceivedMessageErrorCount.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceivedMessageErrorCount.Location = new System.Drawing.Point(6, 33);
            this.lblReceivedMessageErrorCount.Name = "lblReceivedMessageErrorCount";
            this.lblReceivedMessageErrorCount.Size = new System.Drawing.Size(44, 13);
            this.lblReceivedMessageErrorCount.TabIndex = 3;
            this.lblReceivedMessageErrorCount.Text = "Errors:";
            // 
            // tabsMain
            // 
            this.tabsMain.Controls.Add(this.tabStatus);
            this.tabsMain.Controls.Add(this.tabMessages);
            this.tabsMain.Location = new System.Drawing.Point(5, 5);
            this.tabsMain.Margin = new System.Windows.Forms.Padding(1);
            this.tabsMain.Name = "tabsMain";
            this.tabsMain.SelectedIndex = 0;
            this.tabsMain.Size = new System.Drawing.Size(335, 334);
            this.tabsMain.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabsMain.TabIndex = 4;
            this.tabsMain.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabsMain_Selected);
            // 
            // tabStatus
            // 
            this.tabStatus.Controls.Add(this.txtConnectedClientCount);
            this.tabStatus.Controls.Add(this.lblConnectedClientCount);
            this.tabStatus.Controls.Add(this.txtWebExtConnectionStatus);
            this.tabStatus.Controls.Add(this.lblWebExtConnectionStatus);
            this.tabStatus.Controls.Add(this.txtLastMessageReceived);
            this.tabStatus.Controls.Add(this.lblLastMessageReceived);
            this.tabStatus.Controls.Add(this.txtReceivedMessageErrorCount);
            this.tabStatus.Controls.Add(this.lblReceivedMessageCount);
            this.tabStatus.Controls.Add(this.lblReceivedMessageErrorCount);
            this.tabStatus.Controls.Add(this.txtReceivedMessageCount);
            this.tabStatus.Location = new System.Drawing.Point(4, 22);
            this.tabStatus.Name = "tabStatus";
            this.tabStatus.Padding = new System.Windows.Forms.Padding(3);
            this.tabStatus.Size = new System.Drawing.Size(327, 308);
            this.tabStatus.TabIndex = 0;
            this.tabStatus.Text = "Status";
            this.tabStatus.UseVisualStyleBackColor = true;
            // 
            // txtConnectedClientCount
            // 
            this.txtConnectedClientCount.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtConnectedClientCount.Location = new System.Drawing.Point(168, 108);
            this.txtConnectedClientCount.Name = "txtConnectedClientCount";
            this.txtConnectedClientCount.Size = new System.Drawing.Size(126, 13);
            this.txtConnectedClientCount.TabIndex = 11;
            this.txtConnectedClientCount.Text = "0";
            this.txtConnectedClientCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblConnectedClientCount
            // 
            this.lblConnectedClientCount.AutoSize = true;
            this.lblConnectedClientCount.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnectedClientCount.Location = new System.Drawing.Point(6, 108);
            this.lblConnectedClientCount.Name = "lblConnectedClientCount";
            this.lblConnectedClientCount.Size = new System.Drawing.Size(110, 13);
            this.lblConnectedClientCount.TabIndex = 9;
            this.lblConnectedClientCount.Text = "Clients connected:";
            // 
            // txtWebExtConnectionStatus
            // 
            this.txtWebExtConnectionStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtWebExtConnectionStatus.Location = new System.Drawing.Point(168, 83);
            this.txtWebExtConnectionStatus.Name = "txtWebExtConnectionStatus";
            this.txtWebExtConnectionStatus.Size = new System.Drawing.Size(126, 13);
            this.txtWebExtConnectionStatus.TabIndex = 8;
            this.txtWebExtConnectionStatus.Text = "Unknown";
            this.txtWebExtConnectionStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblWebExtConnectionStatus
            // 
            this.lblWebExtConnectionStatus.AutoSize = true;
            this.lblWebExtConnectionStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWebExtConnectionStatus.Location = new System.Drawing.Point(6, 83);
            this.lblWebExtConnectionStatus.Name = "lblWebExtConnectionStatus";
            this.lblWebExtConnectionStatus.Size = new System.Drawing.Size(159, 13);
            this.lblWebExtConnectionStatus.TabIndex = 7;
            this.lblWebExtConnectionStatus.Text = "Web extension connection:";
            // 
            // txtLastMessageReceived
            // 
            this.txtLastMessageReceived.Location = new System.Drawing.Point(152, 58);
            this.txtLastMessageReceived.Name = "txtLastMessageReceived";
            this.txtLastMessageReceived.Size = new System.Drawing.Size(142, 13);
            this.txtLastMessageReceived.TabIndex = 6;
            this.txtLastMessageReceived.Text = "-";
            this.txtLastMessageReceived.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLastMessageReceived
            // 
            this.lblLastMessageReceived.AutoSize = true;
            this.lblLastMessageReceived.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastMessageReceived.Location = new System.Drawing.Point(6, 58);
            this.lblLastMessageReceived.Name = "lblLastMessageReceived";
            this.lblLastMessageReceived.Size = new System.Drawing.Size(140, 13);
            this.lblLastMessageReceived.TabIndex = 5;
            this.lblLastMessageReceived.Text = "Last message received:";
            // 
            // txtReceivedMessageErrorCount
            // 
            this.txtReceivedMessageErrorCount.Location = new System.Drawing.Point(198, 33);
            this.txtReceivedMessageErrorCount.Name = "txtReceivedMessageErrorCount";
            this.txtReceivedMessageErrorCount.Size = new System.Drawing.Size(96, 13);
            this.txtReceivedMessageErrorCount.TabIndex = 4;
            this.txtReceivedMessageErrorCount.Text = "0";
            this.txtReceivedMessageErrorCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabMessages
            // 
            this.tabMessages.Controls.Add(this.chkAutoRefresh);
            this.tabMessages.Controls.Add(this.lstLastReceivedMessages);
            this.tabMessages.Location = new System.Drawing.Point(4, 22);
            this.tabMessages.Name = "tabMessages";
            this.tabMessages.Padding = new System.Windows.Forms.Padding(3);
            this.tabMessages.Size = new System.Drawing.Size(327, 308);
            this.tabMessages.TabIndex = 1;
            this.tabMessages.Text = "Messages";
            this.tabMessages.UseVisualStyleBackColor = true;
            // 
            // chkAutoRefresh
            // 
            this.chkAutoRefresh.AutoSize = true;
            this.chkAutoRefresh.Checked = true;
            this.chkAutoRefresh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoRefresh.Location = new System.Drawing.Point(6, 285);
            this.chkAutoRefresh.Name = "chkAutoRefresh";
            this.chkAutoRefresh.Size = new System.Drawing.Size(87, 17);
            this.chkAutoRefresh.TabIndex = 1;
            this.chkAutoRefresh.Text = "Auto refresh";
            this.chkAutoRefresh.UseVisualStyleBackColor = true;
            this.chkAutoRefresh.CheckedChanged += new System.EventHandler(this.chkAutoRefresh_CheckedChanged);
            // 
            // lstLastReceivedMessages
            // 
            this.lstLastReceivedMessages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colMessageTime,
            this.colMessageEvent});
            this.lstLastReceivedMessages.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem2});
            this.lstLastReceivedMessages.Location = new System.Drawing.Point(3, 6);
            this.lstLastReceivedMessages.Name = "lstLastReceivedMessages";
            this.lstLastReceivedMessages.Size = new System.Drawing.Size(318, 273);
            this.lstLastReceivedMessages.TabIndex = 0;
            this.lstLastReceivedMessages.UseCompatibleStateImageBehavior = false;
            this.lstLastReceivedMessages.View = System.Windows.Forms.View.Details;
            // 
            // colMessageTime
            // 
            this.colMessageTime.Text = "Time";
            this.colMessageTime.Width = 80;
            // 
            // colMessageEvent
            // 
            this.colMessageEvent.Text = "Event";
            this.colMessageEvent.Width = 100;
            // 
            // chkStayOnTop
            // 
            this.chkStayOnTop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkStayOnTop.AutoSize = true;
            this.chkStayOnTop.Location = new System.Drawing.Point(256, 343);
            this.chkStayOnTop.Name = "chkStayOnTop";
            this.chkStayOnTop.Size = new System.Drawing.Size(82, 17);
            this.chkStayOnTop.TabIndex = 5;
            this.chkStayOnTop.Text = "Stay on top";
            this.chkStayOnTop.UseVisualStyleBackColor = true;
            this.chkStayOnTop.CheckedChanged += new System.EventHandler(this.chkStayOnTop_CheckedChanged);
            // 
            // chkKeepOpen
            // 
            this.chkKeepOpen.AutoSize = true;
            this.chkKeepOpen.Location = new System.Drawing.Point(12, 343);
            this.chkKeepOpen.Name = "chkKeepOpen";
            this.chkKeepOpen.Size = new System.Drawing.Size(77, 17);
            this.chkKeepOpen.TabIndex = 6;
            this.chkKeepOpen.Text = "Keep open";
            this.chkKeepOpen.UseVisualStyleBackColor = true;
            this.chkKeepOpen.CheckedChanged += new System.EventHandler(this.chkKeepOpen_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 366);
            this.Controls.Add(this.chkKeepOpen);
            this.Controls.Add(this.chkStayOnTop);
            this.Controls.Add(this.tabsMain);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Testware.Web.WebExtensionMessageBroker.Properties.Resources.applicationIcon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShowInTaskbar = false;
            this.Text = "Web Extension Message Broker";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.appNotifyIconContextMenuStrip.ResumeLayout(false);
            this.tabsMain.ResumeLayout(false);
            this.tabStatus.ResumeLayout(false);
            this.tabStatus.PerformLayout();
            this.tabMessages.ResumeLayout(false);
            this.tabMessages.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon appNotifyIcon;
        private System.Windows.Forms.ContextMenuStrip appNotifyIconContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem contextMenuItemOpen;
        private System.Windows.Forms.ToolStripMenuItem contextMenuItemClose;
        private System.Windows.Forms.Label lblReceivedMessageCount;
        private System.Windows.Forms.Label lblReceivedMessageErrorCount;
        private System.Windows.Forms.Label lblWebExtConnectionStatus;
        private System.Windows.Forms.Label lblLastMessageReceived;
        private System.Windows.Forms.Label txtReceivedMessageErrorCount;
        private System.Windows.Forms.CheckBox chkStayOnTop;
        private System.Windows.Forms.ColumnHeader colMessageTime;
        private System.Windows.Forms.ColumnHeader colMessageEvent;
        private System.Windows.Forms.Label lblConnectedClientCount;
        internal System.Windows.Forms.Label txtReceivedMessageCount;
        internal System.Windows.Forms.ListView lstLastReceivedMessages;
        internal System.Windows.Forms.CheckBox chkAutoRefresh;
        internal System.Windows.Forms.CheckBox chkKeepOpen;
        internal System.Windows.Forms.TabControl tabsMain;
        internal System.Windows.Forms.TabPage tabStatus;
        internal System.Windows.Forms.TabPage tabMessages;
        internal System.Windows.Forms.Label txtWebExtConnectionStatus;
        internal System.Windows.Forms.Label txtLastMessageReceived;
        internal System.Windows.Forms.Label txtConnectedClientCount;
    }
}

