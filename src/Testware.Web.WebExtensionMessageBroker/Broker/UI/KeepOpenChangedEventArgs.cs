﻿using System;

namespace Testware.Web.WebExtensionMessageBroker.Broker.UI
{
    internal class KeepOpenChangedEventArgs : EventArgs
    {
        public bool KeepOpen { get; private set; }

        public KeepOpenChangedEventArgs(bool keepOpen)
        {
            this.KeepOpen = keepOpen;
        }
    }
}
