﻿using System;
using NamedPipeWrapper;
using Testware.Web.WebExtensionMessageBroker.Message;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Testware.Web.WebExtensionMessageBroker.Common;

namespace Testware.Web.WebExtensionMessageBroker.Broker
{
    internal sealed class BrokerServer : IBrokerServer
    {
        private readonly INamedPipeServer<WebExtensionMessage> _pipeServer;
        private readonly BlockingCollection<WebExtensionMessage> _outgoingMessages;

        public const string PipeServerName = "web_extension_broker_pipe";

        private readonly object _syncLock = new object();

        public bool IsRunning { get; private set; }

        public int ConnectedClients { get { return _pipeServer.ClientsConnected; } }

        public event EventHandler ClientConnected;
        public event EventHandler ClientDisconnected;
        public event EventHandler Stopped;
        public event EventHandler<AsyncExceptionEventArgs> AsyncExceptionThrown;

        #region .ctors

        public BrokerServer(BlockingCollection<WebExtensionMessage> outgoingMessages)
            : this(new NamedPipeServer<WebExtensionMessage>(PipeServerName), outgoingMessages) { }

        internal BrokerServer(INamedPipeServer<WebExtensionMessage> pipeServer, BlockingCollection<WebExtensionMessage> outgoingMessages)
        {
            if (pipeServer == null)
                throw new ArgumentNullException("pipeServer");
            if (outgoingMessages == null)
                throw new ArgumentNullException("outgoingMessages");

            _outgoingMessages = outgoingMessages;

            _pipeServer = pipeServer;
            _pipeServer.ClientConnected += _PipeServer_ClientConnected;
            _pipeServer.ClientDisconnected += _PipeServer_ClientDisconnected;
        }

        #endregion

        #region Event handlers

        private void _PipeServer_ClientConnected(INamedPipeConnection<WebExtensionMessage, WebExtensionMessage> connection)
        {
            this.OnClientConnected(EventArgs.Empty);
        }

        private void _PipeServer_ClientDisconnected(INamedPipeConnection<WebExtensionMessage, WebExtensionMessage> connection)
        {
            this.OnClienDisconnected(EventArgs.Empty);
        }

        #endregion

        #region Event raisers

        private void OnClientConnected(EventArgs e)
        {
            var tmp = this.ClientConnected;

            if (tmp != null)
                tmp(this, e);
        }

        private void OnClienDisconnected(EventArgs e)
        {
            var tmp = this.ClientDisconnected;

            if (tmp != null)
                tmp(this, e);
        }

        private void OnStopped(EventArgs e)
        {
            var tmp = this.Stopped;

            if (tmp != null)
                tmp(this, e);
        }

        private void OnAsyncExceptionThrown(AsyncExceptionEventArgs e)
        {
            var tmp = this.AsyncExceptionThrown;

            if (tmp != null)
                tmp(this, e);
        }

        #endregion

        public void StartAsync(CancellationToken cancellationToken)
        {
            _StartAsync(cancellationToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <remarks>Should only be used for testing</remarks>
        internal Task _StartAsync(CancellationToken cancellationToken)
        {
            lock (_syncLock)
            {
                if (IsRunning)
                    throw new InvalidOperationException("Server is already running.");

                var asyncTask = new Task(() => this.Start(cancellationToken));

                asyncTask.ContinueWith(AsyncExceptionHandler, TaskContinuationOptions.OnlyOnFaulted);

                asyncTask.Start();

                this.IsRunning = true;

                return asyncTask;
            }
        }

        private void Start(CancellationToken cancellationToken)
        {
            _pipeServer.Start();

            try
            {
                WebExtensionMessage message;

                while (_outgoingMessages.TryTake(out message, -1, cancellationToken))
                    _pipeServer.PushMessage(message);
            }
            catch (OperationCanceledException)
            {
                //Cancellation requested by token
            }

            _pipeServer.Stop();

            this.IsRunning = false;
            this.OnStopped(EventArgs.Empty);
        }

        private void AsyncExceptionHandler(Task task)
        {
            var e = new AsyncExceptionEventArgs(task.Exception);

            this.OnAsyncExceptionThrown(e);
        }
    }
}
