﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Testware.Web.WebExtensionMessageBroker.Common;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.Broker
{
    internal sealed class WebExtensionListener : IWebExtensionListener
    {
        private readonly BlockingCollection<WebExtensionMessage> _incomingMessageQueue;
        private readonly BlockingCollection<WebExtensionMessage> _uiMessages;
        private readonly IInputReader _input;

        private readonly object _syncLock = new object();

        public int MessagesReceived { get; private set; }

        public bool IsRunning { get; private set; }

        public event EventHandler ConnectionClosed;
        public event EventHandler MessageReceived;
        public event EventHandler<AsyncExceptionEventArgs> AsyncExceptionThrown;

        public WebExtensionListener(BlockingCollection<WebExtensionMessage> messageQueue, BlockingCollection<WebExtensionMessage> uiMessages)
            : this(new StandardInputReader(), messageQueue, uiMessages) { }

        public WebExtensionListener(IInputReader input, BlockingCollection<WebExtensionMessage> messageQueue, BlockingCollection<WebExtensionMessage> uiMessages)
        {
            if (input == null)
                throw new ArgumentNullException("input");
            if (messageQueue == null)
                throw new ArgumentNullException("messageQueue");
            if (uiMessages == null)
                throw new ArgumentNullException("uiMessages");

            _input = input;
            _incomingMessageQueue = messageQueue;
            _uiMessages = uiMessages;
        }

        private void OnConnectionClosed(EventArgs e)
        {
            var tmp = this.ConnectionClosed;

            if (tmp != null)
                tmp(this, e);
        }

        private void OnMessageReceived(EventArgs e)
        {
            var tmp = this.MessageReceived;

            if (tmp != null)
                tmp(this, e);
        }

        private void OnAsyncExceptionThrown(AsyncExceptionEventArgs e)
        {
            var tmp = this.AsyncExceptionThrown;

            if (tmp != null)
                tmp(this, e);
        }

        public void StartAsync(CancellationToken cancellationToken)
        {
            _StartAsync(cancellationToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <remarks>Should only be used for testing</remarks>
        internal Task _StartAsync(CancellationToken cancellationToken)
        {
            lock (_syncLock)
            {
                if (this.IsRunning)
                    throw new InvalidOperationException("Listener is already running.");

                var asyncTask = new Task(() => this.Start(cancellationToken));

                asyncTask.ContinueWith(AsyncExceptionEventHandler, TaskContinuationOptions.OnlyOnFaulted);

                asyncTask.Start();

                this.IsRunning = true;
            
                return asyncTask;
            }
        }

        private void Start(CancellationToken cancellationToken)
        {
            try
            {
                WebExtensionMessage message;

                while ((message = Read(cancellationToken)) != null)
                {
                     _incomingMessageQueue.TryAdd(message, -1, cancellationToken);
                     _uiMessages.TryAdd(message, -1, cancellationToken);

                     this.MessagesReceived++;
                    
                    this.OnMessageReceived(EventArgs.Empty);
                }

                _incomingMessageQueue.CompleteAdding();
            }
            catch (OperationCanceledException)
            {
                //Cancellation requested by token
            }

            this.IsRunning = false;

            this.OnConnectionClosed(EventArgs.Empty);
        }

        private WebExtensionMessage Read(CancellationToken cancellationToken)
        {
            var s = _input.Read(cancellationToken);

            if (String.IsNullOrEmpty(s))
                return null;

            return this.ProcessMessage(s);
        }

        private WebExtensionMessage ProcessMessage(string data)
        {
            return Serialization.Deserialize(data);
        }

        private void AsyncExceptionEventHandler(Task task) {
            var e = new AsyncExceptionEventArgs(task.Exception);

            this.OnAsyncExceptionThrown(e);
        }
    }
}
