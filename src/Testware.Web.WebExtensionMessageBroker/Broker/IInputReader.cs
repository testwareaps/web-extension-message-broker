﻿using System.Threading;

namespace Testware.Web.WebExtensionMessageBroker.Broker
{
    internal interface IInputReader
    {
        string Read(CancellationToken cancellationToken);
    }
}
