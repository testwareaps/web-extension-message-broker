﻿using System;
using System.IO;
using System.Text;
using System.Threading;

namespace Testware.Web.WebExtensionMessageBroker.Broker
{
    internal sealed class StandardInputReader : IInputReader
    {
        private static string _Read(Stream inputStream)
        {
            char[] buffer;

            using (var reader = new StreamReader(inputStream, new UTF8Encoding()))
            {
                var lengthBytes = new byte[4];

                inputStream.Read(lengthBytes, 0, 4);

                var length = BitConverter.ToInt32(lengthBytes, 0);

                buffer = new char[length];

                if (reader.Peek() < 0)
                    return "";

                reader.Read(buffer, 0, buffer.Length);
            }

            var s = new string(buffer);

            return s;
        }

        internal string Read(Stream inputStream)
        {
            if (inputStream == null)
                throw new ArgumentNullException("inputStream");

            return _Read(inputStream);
        }

        public string Read(CancellationToken cancellationToken)
        {
            var stdIn = Console.OpenStandardInput();

            return _Read(stdIn);
        }
    }
}
