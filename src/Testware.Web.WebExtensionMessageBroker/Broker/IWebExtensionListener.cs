﻿using System;
using Testware.Web.WebExtensionMessageBroker.Common;
namespace Testware.Web.WebExtensionMessageBroker.Broker
{
    public interface IWebExtensionListener
    {
        int MessagesReceived { get; }

        event EventHandler ConnectionClosed;

        event EventHandler MessageReceived;

        event EventHandler<AsyncExceptionEventArgs> AsyncExceptionThrown;

        bool IsRunning { get; }
        
        void StartAsync(System.Threading.CancellationToken cancellationToken);
    }
}
