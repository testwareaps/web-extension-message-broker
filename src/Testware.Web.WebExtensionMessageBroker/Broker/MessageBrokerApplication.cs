﻿using System;
using System.Threading;
using System.Windows.Forms;
using Testware.Web.WebExtensionMessageBroker.Broker.UI;

namespace Testware.Web.WebExtensionMessageBroker.Broker
{
    internal class MessageBrokerApplication
    {
        private bool _keepOpen;

        private readonly IWebExtensionListener _webExtensionListener;
        private readonly IBrokerServer _brokerServer;
        private readonly MainForm _frmMain;

        public MessageBrokerApplication(IWebExtensionListener webExtensionListener, IBrokerServer brokerServer, MainForm frmMain)
        {
            if (webExtensionListener == null)
                throw new ArgumentNullException("webExtensionListener");
            if (brokerServer == null)
                throw new ArgumentNullException("brokerServer");
            if (frmMain == null)
                throw new ArgumentNullException("frmMain");

            _webExtensionListener = webExtensionListener;
            _brokerServer = brokerServer;
            _frmMain = frmMain;

            frmMain.KeepOpenChanged += frmMain_KeepOpenChanged;

#if DEBUG
            _keepOpen = true;
#endif
        }

        public void Start()
        {
            _webExtensionListener.StartAsync(CancellationToken.None);

            _brokerServer.Stopped += server_Stopped;
            _brokerServer.StartAsync(CancellationToken.None);

            Application.Run(_frmMain);
        }

        private void frmMain_KeepOpenChanged(object sender, KeepOpenChangedEventArgs e)
        {
            _keepOpen = e.KeepOpen;
        }

        private void server_Stopped(object sender, EventArgs e)
        {
            if (!_keepOpen)
                Environment.Exit(0);
        }
    }
}
