﻿using System;
using Testware.Web.WebExtensionMessageBroker.Common;

namespace Testware.Web.WebExtensionMessageBroker.Broker
{
    public interface IBrokerServer
    {
        bool IsRunning { get; }

        int ConnectedClients { get; }

        event EventHandler<AsyncExceptionEventArgs> AsyncExceptionThrown;
        event EventHandler ClientConnected;
        event EventHandler ClientDisconnected;
        event EventHandler Stopped;
        
        void StartAsync(System.Threading.CancellationToken cancellationToken);
    }
}
