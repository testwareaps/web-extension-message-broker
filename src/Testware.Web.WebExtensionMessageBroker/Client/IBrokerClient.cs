﻿using System;
using System.Collections.Generic;
using System.Threading;
using Testware.Web.WebExtensionMessageBroker.Common;
using Testware.Web.WebExtensionMessageBroker.Message;
namespace Testware.Web.WebExtensionMessageBroker.Client
{
    public interface IBrokerClient
    {
        int MessageCount { get; }

        event EventHandler Disconnected;

        event EventHandler<AsyncExceptionEventArgs> ErrorOccured;

        IEnumerable<WebExtensionMessage> GetMessages();

        IEnumerable<WebExtensionMessage> GetMessages(CancellationToken cancellationToken);

        WebExtensionMessage GetNextMessage();

        WebExtensionMessage GetNextMessage(CancellationToken cancellationToken);

        void StartAsync();

        void Stop();
    }
}
