﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Testware.Web.WebExtensionMessageBroker.Client
{
    public class MessageBrokerWait
    {
        private readonly IBrokerClient _brokerClient;

        internal MessageBrokerWait(IBrokerClient brokerClient)
        {
            _brokerClient = brokerClient;
        }

        public MessageBrokerWait()
            : this(new BrokerClient()) { }

        public void Wait(TimeSpan waitForIdleTime, TimeSpan timeout)
        {
            if (waitForIdleTime.TotalMilliseconds < 0)
                throw new ArgumentException("idleTime must be >= 0 milliseconds");
            if (timeout <= waitForIdleTime)
                throw new ArgumentException("timeout must be greater than waitForIdleTime");

            var monitor = new IncommingMessageMonitor(_brokerClient);

            _brokerClient.StartAsync();
            monitor.StartAsync();

            var swTimeout = new Stopwatch();
            swTimeout.Start();
            
            var timedOut = true;

            while(swTimeout.Elapsed < timeout) {
                Thread.Sleep(100);

                if (monitor.TimeSinceLastMessage < waitForIdleTime)
                    continue;
                
                timedOut = false;

                break;
            }

            _brokerClient.Stop();

            if(timedOut)
                throw new TimeoutException();
        }

        private class IncommingMessageMonitor
        {
            private readonly IBrokerClient _brokerClient;

            private readonly Stopwatch _timeSinceLastMessage;

            public TimeSpan TimeSinceLastMessage { get { return _timeSinceLastMessage.Elapsed; } }

            public IncommingMessageMonitor(IBrokerClient brokerClient)
            {
                _brokerClient = brokerClient;

                _timeSinceLastMessage = new Stopwatch();
                _timeSinceLastMessage.Start();
            }

            public void StartAsync()
            {
                Task.Factory.StartNew(() =>
                {
                    foreach (var msg in _brokerClient.GetMessages())
                        _timeSinceLastMessage.Restart();
                });
            }
        }
    }
}
