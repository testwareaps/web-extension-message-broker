﻿using NamedPipeWrapper;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Testware.Web.WebExtensionMessageBroker.Broker;
using Testware.Web.WebExtensionMessageBroker.Common;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.Client
{
    public class BrokerClient : IBrokerClient
    {
        #region Declarations

        private readonly INamedPipeClient<WebExtensionMessage> _pipeClient;
        private readonly BlockingCollection<WebExtensionMessage> _incomingQueue;

        #endregion

        #region Properties

        public int MessageCount { get { return _incomingQueue.Count; } }

        #endregion

        #region Events

        public event EventHandler Disconnected;
        public event EventHandler<AsyncExceptionEventArgs> ErrorOccured;

        #endregion

        #region .ctor

        public BrokerClient()
            : this(new NamedPipeClient<WebExtensionMessage>(BrokerServer.PipeServerName), new BlockingCollection<WebExtensionMessage>()) { }

        internal BrokerClient(INamedPipeClient<WebExtensionMessage> pipeClient, BlockingCollection<WebExtensionMessage> incomingQueue)
        {
            if (pipeClient == null)
                throw new ArgumentNullException("pipeClient");
            if (incomingQueue == null)
                throw new ArgumentNullException("incomingQueue");

            _pipeClient = pipeClient;
            _incomingQueue = incomingQueue;
        }

        #endregion

        #region Event raisers

        private void OnDisconnected(EventArgs e)
        {
            var tmp = this.Disconnected;

            if (tmp != null)
                tmp(this, e);
        }

        private void OnErrorOccured(AsyncExceptionEventArgs e)
        {
            var tmp = this.ErrorOccured;

            if (tmp != null)
                tmp(this, e);
        }

        #endregion

        #region Event handlers

        private void _PipeClient_Error(Exception exception)
        {
            this.OnErrorOccured(new AsyncExceptionEventArgs(exception));
        }

        private void _PipeClient_Disconnected(INamedPipeConnection<WebExtensionMessage, WebExtensionMessage> connection)
        {
            _incomingQueue.CompleteAdding();

            this.OnDisconnected(EventArgs.Empty);
        }

        private void _PipeClient_ServerMessage(INamedPipeConnection<WebExtensionMessage, WebExtensionMessage> connection, WebExtensionMessage message)
        {
            _incomingQueue.TryAdd(message);
        }

        #endregion

        #region Public methods

        public void StartAsync()
        {
            _pipeClient.ServerMessage += _PipeClient_ServerMessage;
            _pipeClient.Disconnected += _PipeClient_Disconnected;
            _pipeClient.Error += _PipeClient_Error;

            _pipeClient.Start();
        }

        public void Stop()
        {
            _pipeClient.Stop();
            _pipeClient.WaitForDisconnection();

            _incomingQueue.CompleteAdding();
        }

        public IEnumerable<WebExtensionMessage> GetMessages()
        {
            return this.GetMessages(CancellationToken.None);
        }
        public IEnumerable<WebExtensionMessage> GetMessages(CancellationToken cancellationToken)
        {
            WebExtensionMessage message;
            
            while (_incomingQueue.TryTake(out message, -1, cancellationToken))
                yield return message;
        }

        public WebExtensionMessage GetNextMessage()
        {
            return GetNextMessage(CancellationToken.None);
        }
        public WebExtensionMessage GetNextMessage(CancellationToken cancellationToken)
        {
            return GetMessages(cancellationToken).First();
        }

        #endregion
    }
}