﻿using Microsoft.Win32;
using System.IO;
using Testware.Web.Core.WebDriver;

namespace Testware.Web.WebExtensionMessageBroker.Client
{
    public class ChromeWebExtensionInitializer : IExtensionInitializer
    {
        public string GetLocation()
        {
            return Directory.GetCurrentDirectory() + @"\WebExtension\";
        }

        public void Register()
        {
            var chromeKey = Registry.CurrentUser
                .OpenSubKey("Software")
                .OpenSubKey("Google")
                .OpenSubKey("Chrome", true);

            var nativeMessagingKey = chromeKey.CreateSubKey("NativeMessagingHosts");

            nativeMessagingKey.CreateSubKey("testware.web.webextensionmessagebroker")
                .SetValue("", GetAppManifestLocation());
        }

        private string GetAppManifestLocation()
        {
            return Directory.GetCurrentDirectory() + @"\ApplicationManifests\appmanifest_chrome.json";
        }
    }
}
