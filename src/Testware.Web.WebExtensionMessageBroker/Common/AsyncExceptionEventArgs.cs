﻿using System;

namespace Testware.Web.WebExtensionMessageBroker.Common
{
    public class AsyncExceptionEventArgs : EventArgs
    {
        public Exception Exception { get; private set; }

        public AsyncExceptionEventArgs(Exception exception)
        {
            if (exception == null)
                throw new ArgumentNullException("exception");

            this.Exception = exception;
        }
    }
}
