﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace Testware.Web.WebExtensionMessageBroker.Message
{
    [Serializable]
    public class WebExtensionMessage : ObjectFromJson
    {
        private readonly DateTime _received = DateTime.Now;

        private string _documentHtml;

        public DateTime Received { get { return _received; } }

        [JsonProperty("messageType")]
        public string MessageType { get; private set; }
        [JsonProperty("elementXPath")]
        public string ElementXPath { get; private set; }
        [JsonProperty("url")]
        public string Url { get; private set; }
        [JsonProperty("documentHTML")]
        public string DocumentHtml { 
            get {
                return _documentHtml;
            }
            private set
            {
                _documentHtml = value != null ? Regex.Unescape(value) : null;
            }
        }

        [JsonProperty("mutations")]
        public IEnumerable<Mutation> Mutations { get; private set; }

        internal WebExtensionMessage() { }
        internal WebExtensionMessage(string messageType, string elementXPath, string url, string documentHtml, IEnumerable<Mutation> mutations) 
        {
            this.MessageType = messageType;
            this.ElementXPath = elementXPath;
            this.Url = url;
            this.DocumentHtml = documentHtml;
            this.Mutations = mutations;
        }
    }
}