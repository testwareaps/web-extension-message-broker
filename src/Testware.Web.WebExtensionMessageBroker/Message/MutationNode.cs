﻿using Newtonsoft.Json;
using System;

namespace Testware.Web.WebExtensionMessageBroker.Message
{
    [Serializable]
    public class MutationNode : ObjectFromJson
    {
        [JsonProperty("nodeXPath")]
        public string NodeXPath { get; private set; }
    }
}
