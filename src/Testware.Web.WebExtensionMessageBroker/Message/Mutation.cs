﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Testware.Web.WebExtensionMessageBroker.Message
{
    [Serializable]
    public class Mutation : ObjectFromJson
    {
        [JsonProperty("type")]
        public string Type { get; private set; }
        [JsonProperty("targetXPath")]
        public string TargetXPath { get; private set; }
        [JsonProperty("addedNodes")]
        public IEnumerable<MutationNode> AddedNodes { get; private set; }
        [JsonProperty("removedNodes")]
        public IEnumerable<MutationNode> RemovedNodes { get; private set; }
        [JsonProperty("attributeName")]
        public string AttributeName { get; private set; }
        [JsonProperty("attributeNewValue")]
        public string AttributeNewValue { get; private set; }
        [JsonProperty("attributeOldValue")]
        public string AttributeOldValue { get; private set; }
    }
}
