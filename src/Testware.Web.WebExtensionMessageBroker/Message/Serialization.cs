﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Testware.Web.WebExtensionMessageBroker.Message
{
    internal static class Serialization
    {
        private static readonly JsonSerializerSettings Settings;

        static Serialization()
        {
            Settings = new JsonSerializerSettings { Error = SerializationError };
        }

        private static void SerializationError(object sender, ErrorEventArgs e) {
            var objFromJson = e.ErrorContext.OriginalObject as ObjectFromJson;

            if (objFromJson == null) 
                return;

            objFromJson.AddDeserializationError(e.ErrorContext.Error.Message);

            e.ErrorContext.Handled = true;
        }

        public static WebExtensionMessage Deserialize(string msg)
        {
            return JsonConvert.DeserializeObject<WebExtensionMessage>(msg, Settings);
        }
    }
}
