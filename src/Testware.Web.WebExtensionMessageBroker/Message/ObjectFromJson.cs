﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Testware.Web.WebExtensionMessageBroker.Message
{
    [Serializable]
    public abstract class ObjectFromJson
    {
        private readonly List<string> _deserializationErrors = new List<string>();

        public IEnumerable<string> DeserializationErrors {
            get { return _deserializationErrors.AsEnumerable(); }
        }

        internal void AddDeserializationError(string errorMessage)
        {
            if (String.IsNullOrWhiteSpace(errorMessage))
                throw new ArgumentException("Cannot be null, empty or white space", errorMessage);

            _deserializationErrors.Add(errorMessage);
        }
    }
}
