﻿using System.Linq;
using NUnit.Framework;
using Testware.Web.WebExtensionMessageBroker.Utils;

namespace Testware.Web.WebExtensionMessageBroker.UnitTests.Utils
{
    public class ConcurrentFixedSizedQueueTests
    {
        private int _queueLimit;
        private ConcurrentFixedSizedQueue<int> _queue;

        [TestFixtureSetUp]
        public void Setup()
        {
            _queueLimit = 20;
            _queue = new ConcurrentFixedSizedQueue<int>(_queueLimit);

            var integers = Enumerable.Range(0, 30).ToList();

            integers.ForEach(i => _queue.Enqueue(i));
        }

        [Test]
        public void AddedMoreThanQueueLimit_ShouldContainQueueLimitOfItems()
        {
            Assert.AreEqual(_queueLimit, _queue.Count);
        }

        [Test]
        public void CopyTo()
        {
            var copyToArray = new int[_queueLimit];

            _queue.CopyTo(copyToArray, 0);

            CollectionAssert.AreEqual(_queue, copyToArray);
        }

        [Test]
        public void ToArray()
        {
            CollectionAssert.AreEqual(Enumerable.Range(10, 20), _queue.ToArray());
        }


    }
}
