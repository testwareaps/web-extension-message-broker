﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using NUnit.Framework;
using Testware.Web.WebExtensionMessageBroker.Broker;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.UnitTests.Broker
{
    public class WebExtensionListenerTests : IDisposable
    {
        private InputMock _input;
        private BlockingCollection<WebExtensionMessage> _messageQueue;
        private BlockingCollection<WebExtensionMessage> _uiMessageQueue;

        private WebExtensionListener _listener;

        [SetUp]
        public void Setup()
        {
            _input = new InputMock();
            _messageQueue = new BlockingCollection<WebExtensionMessage>();
            _uiMessageQueue = new BlockingCollection<WebExtensionMessage>();

            _listener = new WebExtensionListener(_input, _messageQueue, _uiMessageQueue);
        }

        [Test]
        public void ListenerNotStarted_ShouldReturnIsRunningFalse()
        {
            Assert.IsFalse(_listener.IsRunning);
        }

        [Test]
        public void ServerStarted_ShouldReturnIsRunningTrue()
        {
            _listener._StartAsync(CancellationToken.None);

            Assert.IsTrue(_listener.IsRunning);
        }

        [Test]
        public void NumberOfMessagesReceivedFromInput_ShouldEqualNumberReturnedByMessagesReceived()
        {
            var listenerTask = _listener._StartAsync(CancellationToken.None);

            _input.SendInput(5);
            _input.CompleteInput();

            listenerTask.Wait();

            Assert.AreEqual(5, _listener.MessagesReceived);
        }

        [Test]
        public void ServerRanToCompletion_ShouldReturnIsRunningFalse()
        {
            var task = _listener._StartAsync(CancellationToken.None);

            _input.CompleteInput();

            task.Wait();

            Assert.IsFalse(_listener.IsRunning);
        }

        [Test]
        public void Send5Messages_MessageQueuesShouldHave5Messages()
        {
            var task = _listener._StartAsync(CancellationToken.None);

            _input.SendInput(5);
            _input.CompleteInput();

            task.Wait();

            Assert.AreEqual(5, _messageQueue.Count);
            Assert.AreEqual(5, _uiMessageQueue.Count);
        }

        [Test]
        public void FaultyMessageSentFromInput_ShouldRaiseExceptionEvent()
        {
            var task = _listener._StartAsync(CancellationToken.None);

            bool exceptionThrown = false;

            _listener.AsyncExceptionThrown += (s, e) => exceptionThrown = true;

            _input.SendInput("asd");
            _input.CompleteInput();

            try
            {
                task.Wait();
            }
            catch (Exception)
            {
                // ignored
            }

            //Wait for exception
            Thread.Sleep(1000);

            Assert.IsTrue(exceptionThrown);
        }

        [Test]
        public void MessageReceived_ShouldRaiseMessageReceivedEvent()
        {
            var task = _listener._StartAsync(CancellationToken.None);

            bool messageReceivedRaised = false;

            _listener.MessageReceived += (s, e) => messageReceivedRaised = true;

            _input.SendInput("{}");
            _input.CompleteInput();

            task.Wait();

            Assert.IsTrue(messageReceivedRaised);
        }

        [Test]
        public void InputEnded_ShouldRaiseConnectionClosedEvent()
        {
            var task = _listener._StartAsync(CancellationToken.None);

            bool connectionClosedRaised = false;

            _listener.ConnectionClosed += (s, e) => connectionClosedRaised = true;

            _input.CompleteInput();

            task.Wait();

            Assert.IsTrue(connectionClosedRaised);
        }

        [TearDown]
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources  
                if (_input != null)
                {
                    _input.Dispose();
                    _input = null;
                }

                if (_messageQueue != null)
                {
                    _messageQueue.Dispose();
                    _messageQueue= null;
                }

                if (_uiMessageQueue != null)
                {
                    _uiMessageQueue.Dispose();
                    _uiMessageQueue = null;
                }
            }
        }
    }

    public class InputMock : IInputReader, IDisposable
    {
        private BlockingCollection<string> _queue = new BlockingCollection<string>();

        public string Read(CancellationToken cancellationToken)
        {
 	        return _queue.Take();
        }

        public void SendInput(string input)
        {
            _queue.Add(input);
        }

        public void SendInput(int count) {
            for (int i = 0; i < count; i++)
                _queue.Add("{}");
        }

        public void CompleteInput()
        {
            _queue.Add("");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources  
                if (_queue != null)
                {
                    _queue.Dispose();
                    _queue = null;
                }
            }
        }  
    }
}
