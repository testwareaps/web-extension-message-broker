﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using Moq;
using NUnit.Framework;
using Testware.Web.WebExtensionMessageBroker.Broker;
using Testware.Web.WebExtensionMessageBroker.Broker.UI;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.UnitTests.Broker.UI
{
    public class MainFormTests
    {
        private Mock<IWebExtensionListener> _listener;
        private Mock<IBrokerServer> _server;
        private BlockingCollection<WebExtensionMessage> _uiMessages;
        private MainForm _frmMain;

        [SetUp]
        public void Setup()
        {
            _listener = new Mock<IWebExtensionListener>();

            _server = new Mock<IBrokerServer>();

            _uiMessages = new BlockingCollection<WebExtensionMessage>();

            _frmMain = new MainForm(_listener.Object, _server.Object, _uiMessages);
            _frmMain.Show();
        }

        [Test]
        public void chkKeepOpenCheckedChanged_ShouldRaiseKeepOpenChangedEvent()
        {
            bool keepOpenChangedRaised = false;

            _frmMain.KeepOpenChanged += (s, e) => keepOpenChangedRaised = true;

            _frmMain.chkKeepOpen.Checked = !_frmMain.chkKeepOpen.Checked;

            Assert.IsTrue(keepOpenChangedRaised);
        }
    }

    public class MainFormMessageReceivedTests
    {
        private Mock<IWebExtensionListener> _listener;
        private Mock<IBrokerServer> _server;
        private BlockingCollection<WebExtensionMessage> _uiMessages;
        private MainForm _frmMain;

        [TestFixtureSetUp]
        public void Setup()
        {
            _listener = new Mock<IWebExtensionListener>();

            _server = new Mock<IBrokerServer>();
            _server.Setup(s => s.ConnectedClients).Returns(2);

            _uiMessages = new BlockingCollection<WebExtensionMessage>();

            _frmMain = new MainForm(_listener.Object, _server.Object, _uiMessages);
            _frmMain.Show();
            _frmMain.tabsMain.SelectedTab = _frmMain.tabMessages;

            _uiMessages.Add(new WebExtensionMessage("Test1", null, null, null, null));
            _uiMessages.Add(new WebExtensionMessage("Test2", null, null, null, null));
            _uiMessages.Add(new WebExtensionMessage("Test3", null, null, null, null));

            _listener.Setup(l => l.IsRunning).Returns(true);
            _listener.Setup(l => l.MessagesReceived).Returns(_uiMessages.Count);
            _listener.Raise(l => l.MessageReceived += null, EventArgs.Empty);

            _server.Raise(s => s.ClientConnected += null, EventArgs.Empty);
        }

        [Test]
        public void Test()
        {
            Assert.AreEqual(_uiMessages.Count.ToString(), _frmMain.txtReceivedMessageCount.Text);
        }

        [Test]
        public void MainFormShouldShowCorrectTimestampForLastMessageReceived()
        {
            Assert.AreEqual(_uiMessages.Last().Received.ToString("yyyy-MM-dd HH:mm:ss.fff"), _frmMain.txtLastMessageReceived.Text);
        }

        [Test]
        public void MainFormShouldShowCorrectExtensionConnectionStatus()
        {
            Assert.AreEqual("Connected", _frmMain.txtWebExtConnectionStatus.Text);
        } 

        [Test]
        public void MainFormShouldShowCorrectNumberOfConnectedClients()
        {
            Assert.AreEqual(_server.Object.ConnectedClients.ToString(), _frmMain.txtConnectedClientCount.Text);
        }

        [Test]
        public void MessageListViewShouldShowCorrectNumberOfRows()
        {
            Assert.AreEqual(_uiMessages.Count, _frmMain.lstLastReceivedMessages.Items.Count);
        }
    }
}
