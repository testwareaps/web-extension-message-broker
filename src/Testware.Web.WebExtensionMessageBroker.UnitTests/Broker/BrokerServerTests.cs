﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NamedPipeWrapper;
using NUnit.Framework;
using Testware.Web.WebExtensionMessageBroker.Broker;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.UnitTests.Broker
{
    public class BrokerServerTestsWithoutMessages : IDisposable
    {
        private BrokerServer _broker;
        private BlockingCollection<WebExtensionMessage> _messages;

        [SetUp]
        public void Setup()
        {
            var pipeServerMock = new Mock<INamedPipeServer<WebExtensionMessage>>();

            _messages = new BlockingCollection<WebExtensionMessage>();

            _broker = new BrokerServer(pipeServerMock.Object, _messages);
        }

        [Test]
        public void ServerNotStarted_ShouldReturnIsRunningFalse()
        {
            Assert.IsFalse(_broker.IsRunning);
        }

        [Test]
        public void ServerStarted_ShouldReturnIsRunningTrue()
        {
            _broker._StartAsync(CancellationToken.None);

            Assert.IsTrue(_broker.IsRunning);
        }

        [Test]
        public void ServerRanToCompletion_ShouldReturnIsRunningFalseAndRaiseEventStopped()
        {
            var stoppedEventRaised = false;
            _broker.Stopped += (s, e) => stoppedEventRaised = true;

            var brokerTask = _broker._StartAsync(CancellationToken.None);

            _messages.CompleteAdding();

            brokerTask.Wait();

            Assert.IsFalse(_broker.IsRunning);
            Assert.IsTrue(stoppedEventRaised);
        }

        [Test]
        public void ServerCancelledByToken_ShouldReturnIsRunningFalseAndRaiseEventStopped()
        {
            var stoppedEventRaised = false;
            _broker.Stopped += (s, e) => stoppedEventRaised = true;

            var cts = new CancellationTokenSource();

            var brokerTask = _broker._StartAsync(cts.Token);
            
            cts.Cancel();

            brokerTask.Wait();

            Assert.IsFalse(_broker.IsRunning);
            Assert.IsTrue(stoppedEventRaised);
        }

        [TearDown]
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing || _messages == null) 
                return;

            // free managed resources  
            _messages.Dispose();
            _messages = null;
        }
    }

    public class BrokerServerTestsWithMessages : IDisposable
    {
        BlockingCollection<WebExtensionMessage> _serverQueue;
        Mock<INamedPipeServer<WebExtensionMessage>> _pipeServerMock;
        Task _brokerTask;

        [SetUp]
        public void Setup()
        {
            _serverQueue = new BlockingCollection<WebExtensionMessage>();

            _pipeServerMock = new Mock<INamedPipeServer<WebExtensionMessage>>();

            var broker = new BrokerServer(_pipeServerMock.Object, _serverQueue);
            _brokerTask = broker._StartAsync(CancellationToken.None);
        }

        [Test]
        public void Added10MessagesToServerQueue_ShouldCallPushMessages10Times()
        {
            var message = new WebExtensionMessage("Type", null, null, null, null);

            for(int i = 0; i < 10; i++)
                _serverQueue.Add(message);

            _serverQueue.CompleteAdding();
            _brokerTask.Wait();

            _pipeServerMock.Verify(s => s.PushMessage(It.IsAny<WebExtensionMessage>()), Times.Exactly(10));
        }

        [TearDown]
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing || _serverQueue == null) 
                return;

            // free managed resources  
            _serverQueue.Dispose();
            _serverQueue = null;
        }
    }

    public class BrokerServerClientConnectedTests : IDisposable
    {
        private BrokerServer _broker;
        private BlockingCollection<WebExtensionMessage> _serverQueue;
        private Mock<INamedPipeServer<WebExtensionMessage>> _pipeServerMock;
        private Task _brokerTask;

        [SetUp]
        public void Setup()
        {
            _serverQueue = new BlockingCollection<WebExtensionMessage>();

            _pipeServerMock = new Mock<INamedPipeServer<WebExtensionMessage>>();
            _pipeServerMock.Setup(s => s.ClientsConnected).Returns(2);

            _broker = new BrokerServer(_pipeServerMock.Object, _serverQueue);
            _brokerTask = _broker._StartAsync(CancellationToken.None);
        }

        [Test]
        public void PipeServerRaisedClientConnectedEvent_BrokerShouldRaiseClientConnectedEvent()
        {
            var clientConnectedRaised = false;

            _broker.ClientConnected += (o, e) => clientConnectedRaised = true;

            _pipeServerMock.Raise(s => s.ClientConnected += null, (ConnectionEventHandler<WebExtensionMessage, WebExtensionMessage>)null);

            _serverQueue.CompleteAdding();

            _brokerTask.Wait();

            Assert.IsTrue(clientConnectedRaised);
        }

        [Test]
        public void PipeServerRaisedClientDisconnectedEvent_BrokerShouldRaiseClientDisconnectedEvent()
        {
            var clientDisconnectedRaised = false;

            _broker.ClientDisconnected += (o, e) => clientDisconnectedRaised = true;

            _pipeServerMock.Raise(s => s.ClientDisconnected += null, (ConnectionEventHandler<WebExtensionMessage, WebExtensionMessage>)null);

            _serverQueue.CompleteAdding();

            _brokerTask.Wait();

            Assert.IsTrue(clientDisconnectedRaised);
        }

        [Test]
        public void PipeServerReturnsClientCount2_BrokerShouldReturnClientCount2()
        {
            Assert.AreEqual(2, _broker.ConnectedClients);
        }

        [TearDown]
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) 
                return;

            // free managed resources  
            if (_serverQueue != null)
            {
                _serverQueue.Dispose();
                _serverQueue = null;
            }

            if (_serverQueue != null)
            {
                _serverQueue.Dispose();
                _serverQueue = null;
            }
        }
    }

    public class BrokerServerTestsWithError
    {
        //Mock<IProducerConsumerCollection<WebExtensionMessage>> _serverQueueMock;
        //BrokerServer _broker;

        //[SetUp]
        //public void Setup()
        //{
        //    var message = new WebExtensionMessage();

        //    _ServerQueueMock = new Mock<IProducerConsumerCollection<WebExtensionMessage>>();
        //    _ServerQueueMock.Setup(c => c.TryTake(out message))
        //        .OutCallback<IProducerConsumerCollection<WebExtensionMessage>, bool, WebExtensionMessage>((out msg) => msg = message);

        //    _Broker = new BrokerServer(new BlockingCollection<WebExtensionMessage>(_ServerQueueMock.Object));
        //}

        //[Test]
        //public void Test()
        //{
        //    var task = _Broker._StartAsync(CancellationToken.None);

        //    bool asyncExceptionEventThrown = false;

        //    _Broker.AsyncExceptionThrown += (o, e) => asyncExceptionEventThrown = true;

        //    try
        //    {
        //        task.Wait();
        //    }
        //    catch (Exception) { }

        //    Assert.IsTrue(asyncExceptionEventThrown);
        //}
    }
}
