﻿using System;
using System.IO;
using System.Text;
using NUnit.Framework;
using Testware.Web.WebExtensionMessageBroker.Broker;

namespace Testware.Web.WebExtensionMessageBroker.UnitTests.Broker
{
    public class StandardInputReaderTests
    {
        private StandardInputReader _reader;

        [SetUp]
        public void Setup()
        {
            _reader = new StandardInputReader();
        }

        [Test]
        public void ValueSentToInputStream_ShouldEqualValueFromInputReader()
        {
            string output;
            const string input = "test";
            var length = BitConverter.GetBytes(input.Length);

            var inputStream = new MemoryStream();

            using (var writer = new StreamWriter(inputStream, new UTF8Encoding()))
            {
                inputStream.Write(length, 0, 4);

                writer.WriteLine(input);
                writer.Flush();

                inputStream.Position = 0;

                output = _reader.Read(inputStream);
            }

            Assert.AreEqual(input, output);
        }
    }
}
