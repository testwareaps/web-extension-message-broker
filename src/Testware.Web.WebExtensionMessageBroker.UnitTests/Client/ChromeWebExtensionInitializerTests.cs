﻿using System.IO;
using Microsoft.Win32;
using NUnit.Framework;
using Testware.Web.WebExtensionMessageBroker.Client;

namespace Testware.Web.WebExtensionMessageBroker.UnitTests.Client
{
    public class ChromeWebExtensionInitializerTests
    {
        private ChromeWebExtensionInitializer _chromeExtInitializer;

        [TestFixtureSetUp]
        public void Setup()
        {
            GetCurrentUserChromeKey()
                .DeleteSubKeyTree("NativeMessagingHosts", false);

            _chromeExtInitializer = new ChromeWebExtensionInitializer();
        }

        //[Test, Ignore("Chrome not installed on test machines")]
        //public void RegisterShouldCreateCorrectKey()
        //{
        //    _chromeExtInitializer.Register();

        //    var chromeKey = GetCurrentUserChromeKey();

        //    var extensionKey = chromeKey.OpenSubKey("NativeMessagingHosts").OpenSubKey("testware.web.webextensionmessagebroker");

        //    var expectedValue = Directory.GetCurrentDirectory() + @"\ApplicationManifests\appmanifest_chrome.json";

        //    Assert.AreEqual(expectedValue, extensionKey.GetValue(""));
        //}

        private static RegistryKey GetCurrentUserChromeKey()
        {
            return Registry.CurrentUser
                .OpenSubKey("Software")
                .OpenSubKey("Google")
                .OpenSubKey("Chrome", true);
        }
    }
}
