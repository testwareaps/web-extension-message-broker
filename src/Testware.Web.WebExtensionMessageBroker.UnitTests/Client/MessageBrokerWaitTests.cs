﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Testware.Web.WebExtensionMessageBroker.Client;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.UnitTests.Client
{
    public class MessageBrokerWaitTests
    {
        private Mock<IBrokerClient> _brokerClientMock;
        private BlockingCollection<WebExtensionMessage> _messageQueue;

        private MessageBrokerWait _wait;

        [SetUp]
        public void Setup()
        {
            _brokerClientMock = new Mock<IBrokerClient>();
            _messageQueue = new BlockingCollection<WebExtensionMessage>();

            _brokerClientMock.Setup(c => c.GetMessages()).Returns(_messageQueue.GetConsumingEnumerable());

            _wait = new MessageBrokerWait(_brokerClientMock.Object);
        }

        [Test]
        public void TimeoutExceededShouldThrowTimeoutException()
        {
            var cts = new CancellationTokenSource();

            var addTask = Task.Factory.StartNew(() =>
            {
                while (!cts.IsCancellationRequested)
                {
                    Thread.Sleep(1000);

                    _messageQueue.Add(new WebExtensionMessage());
                }
            });

            var sw = new Stopwatch();
            sw.Start();

            Assert.Throws<TimeoutException>(() => _wait.Wait(TimeSpan.FromSeconds(3), TimeSpan.FromSeconds(8)));

            sw.Stop();
            cts.Cancel();

            addTask.Wait();

            var expectedMinimumWaitTime = TimeSpan.FromSeconds(8);

            Assert.GreaterOrEqual(sw.Elapsed, expectedMinimumWaitTime);
        }

        [Test]
        public void WaitShouldNotThrowExceptionAndElapsedTimeBeforeWaitReturnsShouldBeAFunctionOfMessagesReceived()
        {
            const int secondsBetweenEachMessage = 1;
            const int numberOfMessages = 5;
            const int secondsWaitForIdle = 3;

            Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < numberOfMessages; i++)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(secondsBetweenEachMessage));

                    _messageQueue.Add(new WebExtensionMessage());
                }
            });

            var sw = new Stopwatch();
            sw.Start();

            Assert.DoesNotThrow(() => _wait.Wait(TimeSpan.FromSeconds(secondsWaitForIdle), TimeSpan.FromMinutes(1)));

            sw.Stop();
            var expectedMinimumWaitTime = TimeSpan.FromSeconds((secondsBetweenEachMessage * numberOfMessages) + secondsWaitForIdle);

            Assert.GreaterOrEqual(sw.Elapsed, expectedMinimumWaitTime);
        }

        [Test]
        public void Reuse()
        {
            const int secondsBetweenEachMessage = 1;
            const int numberOfMessages = 5;
            const int secondsWaitForIdle = 3;

            Task.Factory.StartNew(() =>
            {
                for (var i = 0; i < numberOfMessages; i++)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(secondsBetweenEachMessage));

                    _messageQueue.Add(new WebExtensionMessage());
                }
            });

            _wait.Wait(TimeSpan.FromSeconds(secondsWaitForIdle), TimeSpan.FromMinutes(1));

            Task.Factory.StartNew(() =>
            {
                for (var i = 0; i < numberOfMessages; i++)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(secondsBetweenEachMessage));

                    _messageQueue.Add(new WebExtensionMessage());
                }
            });

            Assert.DoesNotThrow(() => _wait.Wait(TimeSpan.FromSeconds(secondsWaitForIdle), TimeSpan.FromMinutes(1)));
        }
    }
}
