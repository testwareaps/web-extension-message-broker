﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NamedPipeWrapper;
using NUnit.Framework;
using Testware.Web.WebExtensionMessageBroker.Client;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.UnitTests.Client
{
    public class BrokerClientTests : IDisposable
    {
        private Mock<INamedPipeClient<WebExtensionMessage>> _pipeClientMock;
        private BrokerClient _client;
        private BlockingCollection<WebExtensionMessage> _clientQueue;

        [SetUp]
        public void Setup()
        {
            _pipeClientMock = new Mock<INamedPipeClient<WebExtensionMessage>>();
            _clientQueue = new BlockingCollection<WebExtensionMessage>();

            _client = new BrokerClient(_pipeClientMock.Object, _clientQueue);
        }

        [Test]
        public void ClientDisconnection_ShouldRaiseEventDisconnected()
        {
            bool disconnectedRaised = false;

            _client.Disconnected += (s, e) => disconnectedRaised = true;
            _client.StartAsync();

            Mock<INamedPipeConnection<WebExtensionMessage, WebExtensionMessage>> connectionMock = new Mock<INamedPipeConnection<WebExtensionMessage,WebExtensionMessage>>();

            _pipeClientMock.Raise(c => c.Disconnected += null, connectionMock.Object);

            Assert.IsTrue(disconnectedRaised);
        }

        [Test]
        public void ClientError_ShouldRaiseEventErrorOccured()
        {
            bool errorOccuredRaised = false;

            _client.ErrorOccured += (s, e) => errorOccuredRaised = true;
            _client.StartAsync();

            _pipeClientMock.Raise(c => c.Error += null, new Exception());

            Assert.IsTrue(errorOccuredRaised);
        }

        [Test]
        public void ClientReceived5Messages_ShouldReturn5Messages()
        {
            _client.StartAsync();

            Mock<INamedPipeConnection<WebExtensionMessage, WebExtensionMessage>> connectionMock = new Mock<INamedPipeConnection<WebExtensionMessage,WebExtensionMessage>>();

            for(int i = 0; i < 5; i++)
                _pipeClientMock.Raise(c => c.ServerMessage += null, connectionMock.Object, new WebExtensionMessage());

            _client.Stop();

            Assert.AreEqual(5, _client.GetMessages(CancellationToken.None).Count());
        }

        [Test]
        public void ClientGetMessage_ShouldReturnSingleMessage()
        {
            _client.StartAsync();

            Mock<INamedPipeConnection<WebExtensionMessage, WebExtensionMessage>> connectionMock = new Mock<INamedPipeConnection<WebExtensionMessage, WebExtensionMessage>>();

            var message = new WebExtensionMessage();
            _pipeClientMock.Raise(c => c.ServerMessage += null, connectionMock.Object, message);

            _client.Stop();

            Assert.AreSame(message, _client.GetNextMessage());
        }

        [Test]
        public void ClientGetNextMessageCancelled_ShouldThrowException()
        {
            _client.StartAsync();

            bool cancelledExceptionThrown = false;

            var cts = new CancellationTokenSource();

            var getMessagesTask = Task.Factory.StartNew(() => {
                try {
                    _client.GetNextMessage(cts.Token);
                } catch(OperationCanceledException) {
                    cancelledExceptionThrown = true;
                }
            });

            cts.Cancel();

            getMessagesTask.Wait(5000);

            Assert.IsTrue(cancelledExceptionThrown, "Expected OperationCancelledException to be thrown");
        }

        [Test]
        public void ClientGetMessagesCancelled_ShouldCompleteWithoutThrowingException()
        {
            _client.StartAsync();

            bool cancelledExceptionThrown = false;

            var cts = new CancellationTokenSource();

            var getMessagesTask = Task.Factory.StartNew(() =>
            {
                try
                {
                    // ReSharper disable once IteratorMethodResultIsIgnored
                    _client.GetMessages(cts.Token);
                }
                catch (OperationCanceledException)
                {
                    cancelledExceptionThrown = true;
                }
            });

            cts.Cancel();

            var completedTask = getMessagesTask.Wait(5000);

            Assert.IsTrue(completedTask, "Expected get messages task to complete withing 5 seconds");
            Assert.IsFalse(cancelledExceptionThrown, "Did not expect OperationCancelledException to be thrown");
        }

        [TearDown]
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing || _clientQueue == null) 
                return;

            // free managed resources  
            _clientQueue.Dispose();
            _clientQueue = null;
        }
    }
}
