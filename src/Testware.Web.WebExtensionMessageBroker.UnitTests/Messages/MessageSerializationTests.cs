﻿using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using NUnit.Framework;
using Testware.Web.WebExtensionMessageBroker.Message;

namespace Testware.Web.WebExtensionMessageBroker.UnitTests.Messages
{
    public class MessageSerializationFromJsonTests
    {
        private string _jsonMessage;
        private WebExtensionMessage _message;

        [TestFixtureSetUp]
        public void Setup()
        {
            _jsonMessage = @"{""messageType"":""messageType"",""elementXPath"":""elementXPath"",""url"":""url"",""documentHTML"":""<html><body id=\""bodyId\""></body></html>"",""mutations"":[{""type"":""type"",""addedNodes"":[{""nodeXPath"":""addedNode""}],""removedNodes"":[{""nodeXPath"":""removedNode""}],""attributeName"":""attributeName"",""targetXPath"":""targetXPath"",""attributeNewValue"":""attributeNewValue"",""attributeOldValue"":""attributeOldValue""}]}";

            _message = Serialization.Deserialize(_jsonMessage);
        }

        [Test]
        public void MessageType()
        {
            Assert.AreEqual("messageType", _message.MessageType);
        }

        [Test]
        public void ElementXPath()
        {
            Assert.AreEqual("elementXPath", _message.ElementXPath);
        }

        [Test]
        public void Url()
        {
            Assert.AreEqual("url", _message.Url);
        }

        [Test]
        public void DocumentHtml()
        {
            Assert.AreEqual(@"<html><body id=""bodyId""></body></html>", _message.DocumentHtml);
        }

        [Test]
        public void MutationType()
        {
            Assert.AreEqual("type", _message.Mutations.First().Type);
        }

        [Test]
        public void MutationAddedNodes()
        {
            Assert.AreEqual("addedNode", _message.Mutations.First().AddedNodes.First().NodeXPath);
        }

        [Test]
        public void MutationRemovedNodes()
        {
            Assert.AreEqual("removedNode", _message.Mutations.First().RemovedNodes.First().NodeXPath);
        }

        [Test]
        public void MutationAttributeName()
        {
            Assert.AreEqual("attributeName", _message.Mutations.First().AttributeName);
        }

        [Test]
        public void MutationTargetXPath()
        {
            Assert.AreEqual("targetXPath", _message.Mutations.First().TargetXPath);
        }

        [Test]
        public void MutationAttributeNewValue()
        {
            Assert.AreEqual("attributeNewValue", _message.Mutations.First().AttributeNewValue);
        }

        [Test]
        public void MutationAttributeOldValue()
        {
            Assert.AreEqual("attributeOldValue", _message.Mutations.First().AttributeOldValue);
        }
    }

    public class MessageSerializationFromJsonWithErrorsTests
    {
        private string _jsonMessage;
        private WebExtensionMessage _message;

        [TestFixtureSetUp]
        public void Setup()
        {
            _jsonMessage = @"{""mmessageType"":""messageType"",""elementXPath"":""elementXPath"",""url"":""url"",""documentHTML"":""documentHTML"",""mutations"":[{""type"":""type"",""addedNodes"":""nodeXPath"",""removedNodes"":[{""nodeXPath"":""removedNode""}],""attributeName"":""attributeName"",""targetXPath"":""targetXPath"",""attributeNewValue"":""attributeNewValue"",""attributeOldValue"":""attributeOldValue""}]}";

            _message = Serialization.Deserialize(_jsonMessage);
        }

        [Test]
        public void FirstMutationOfMessage_ShouldContainOneError()
        {
            Assert.AreEqual(1, _message.Mutations.First().DeserializationErrors.Count(), "Expected one error from addedNodes not being designated as an array in json string.");
        }
    }

    public class MessageSerializationToStream
    {
        [Test]
        public void SerializaMessageToStream_ShouldNotThrow()
        {
            var message = new WebExtensionMessage();
            var formatter = new BinaryFormatter();

            using (var memoryStream = new MemoryStream())
            {
                Assert.DoesNotThrow(() => formatter.Serialize(memoryStream, message));
            }
        }
    }
}
