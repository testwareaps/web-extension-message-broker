﻿using System.Reflection;

[assembly: AssemblyCompany("Testware ApS")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyProduct("Testware.Web.WebExtensionMessageBroker")]

// Version information for an assembly consists of the following four values:
[assembly: AssemblyVersion("1.0.0")]